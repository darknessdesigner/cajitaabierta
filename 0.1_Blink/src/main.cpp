#include <Arduino.h>

// Le code de base de cet exemple se trouve dans Arduino sous 'Fichier -> exemples -> 01.Basics -> Blink
// Exercice : tentez de changer la vitesse de clignotement
// Exercice avancé : Faites correspondre le clignotement à une lettre de code Morse :)

// Cette partie du code est exécutée une fois au moment de la mise sous tension
void setup() {
  // initialisation de la broche LED_BUILTIN en tant que sortie.
  // Led_BUILTIN est une variable définie par l'environnement Arduino.
  pinMode(LED_BUILTIN, OUTPUT);
}

// Cette partie du code est exécutée en boucle
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // Allume la DEL (HIGH est le niveau de tension (3.3V))
  delay(1000);                       // Attendre une seconde, le paramètre '1000' correspond à 1000 milièmes de secondes 
  digitalWrite(LED_BUILTIN, LOW);    // Éteindre la DEL en amenant la tension à LOW (0V)
  delay(1000);                       // Attendre une seconde
}
