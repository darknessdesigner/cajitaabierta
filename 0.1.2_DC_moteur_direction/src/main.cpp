#include <Arduino.h>

// Image : https://wiki.eavmuqam.ca/moteur_dc_branchements_avec_un_controleur_l298n_et_arduino
// Exemple précédent : Contrôle de base d'un moteur DC (Direct Current).  0.1_DC_simple
// Exercice : Tentez d'arrêter le moteur, de faire une pause, puis de le faire tourner dans l'autre direction

void setup() {
  // initialiser les broches 32 et 33 sont configurées en tant que sorties
  pinMode(32, OUTPUT);
  pinMode(33, OUTPUT);
}

void loop(){
  // Nous envoyons une tension positive et l'autre est à 0V, ceci fera tourner le moteur en une direction
  digitalWrite(32, HIGH);  // <-- Tension de 3.3V  ('HIGH' équivaut à ce voltage)
  digitalWrite(33, LOW); // <-- Tension de 0V

  delay(4000);

  // Les deux sont à 0V, ceci mettra le moteur en arrêt
  digitalWrite(32, LOW);  // <-- Tension de 0V
  digitalWrite(33, LOW); // <-- Tension de 0V

  delay(2000);

    // Nous envoyons une tension positive et l'autre est à 0V, ceci fera tourner le moteur dans l'autre direction
  digitalWrite(32, LOW);  // <-- Tension de 3.3V  ('HIGH' équivaut à ce voltage)
  digitalWrite(33, HIGH); // <-- Tension de 0V

  delay(2000);

  // Les deux sont à 0V, ceci mettra le moteur en arrêt
  digitalWrite(32, LOW);  // <-- Tension de 0V
  digitalWrite(33, LOW); // <-- Tension de 0V

  delay(2000);
 } 

 // Comment faire correspondre la vitesse du moteur à la lecture d'un potentiomètre?
 // Contrôle du moteur DC avec analogWrite(); voir 0.1.3_DC_moteur_progressif
