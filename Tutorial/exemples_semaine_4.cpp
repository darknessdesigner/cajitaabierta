// Les parties 

// 1. Déclaration de variables
pinMode(32, OUTPUT); // initialise la broche 32 en mode sortie
pinMode(14, INPUT); // initialise la broche 14 en mode entrée

// 2. Fonction setup(){}, la fonction de paramètrage du code, exécuté une seule fois
void setup()
{
Serial.begin(115200); // initialise la communication par le port série
}

// 3. Fonction loop(){}, la fonction où nous mettons le code qui est exécuté en boucle
void loop()
{
digitalWrite(32, HIGH); // envoie une tension de 3.3v à la broche 32 
analogRead(14); // lit la tension à la broche 14   
Serial.println("Bonjour le monde, halo welt, holá mundo, hello world"); // affiche le texte
}

// Combien de fonctions utilisons-nous ici? 
// Comment pouvons-nous afficher la valeur de la tension lue par analogRead à la broche 14?


