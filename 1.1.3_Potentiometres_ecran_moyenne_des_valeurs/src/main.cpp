/* Comment réduire le bruit du capteur? */
/* Au préalable : comprendre les exemples 0_2.Tableau et 0.2.1_Tableau moyenne des valeurs */

#include <Arduino.h>
#include <Wire.h> // Communication par i2c
#include <Adafruit_GFX.h> // Librairie graphique pour l'écran
#include <Adafruit_SSD1306.h> // On ajoute la librairie de l'écran SSD1306

// Pour lisser les valeurs d'un capteur
int compteur = 0;
int monTableau[10]; // déclaration d'un tableau de 10 valeurs
int monTotal;
int maMoyenne;

// Crée une instance de l'écran
#define OLED_SDA_PIN 21 // Pour la communication i2c
#define OLED_SCL_PIN 22
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

// Variables dans lesquelles nous garderons les valeurs des potentiomètres
int pot1 = 0;
int pot2 = 0;


void setup() {
  Serial.begin(115200); // Initialisation de la communication sérielle

  /// Écran ///
    Wire.begin(OLED_SDA_PIN, OLED_SCL_PIN);
   
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
        Serial.println(F("SSD1306 allocation failed"));
    }
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println(F("1.1.3 Potentiometre"));
    display.display();      // Show initial text
    delay(300);
}

void loop() {

    pot1 = analogRead(14);
    //pot2 = analogRead(26);

    Serial.print(">pot1:");Serial.println(pot1);
    //Serial.print(">pot2:");Serial.println(pot2);

    // Ajouter la valeur de pot1 aux valeurs de monTableau à la position indiquée par le compteur
    monTableau[compteur] = pot1;

    // Faire la moyenne des valeurs de monTableau (révision à l'exemple 0.2.1_moyenne d'un tableau)
    for (int i = 0; i < (sizeof(monTableau) / sizeof(monTableau[0])); i++) {
        monTotal = monTableau[i] + monTotal;
    }
    maMoyenne = monTotal / (sizeof(monTableau) / sizeof(monTableau[0])); // Fait la moyenne
    monTotal = 0; // Remise de monTotal à 0

    Serial.print(">maMoyenne:");
    Serial.println(maMoyenne);


    // Afficher la valeur de la moyenne à l'écran //
    display.clearDisplay(); // Efface l'image précédente
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.print(F("1.1.3 Potentiometre"));
    display.setTextSize(2);
    display.setCursor(5,20);
    display.print(F("pot1:"));
    display.setCursor(65,20);
    display.print(pot1);
    display.setCursor(5,40);
    display.print(F("moy:"));
    display.setCursor(65,40);
    display.print(maMoyenne);
    display.display();      // Show initial text

    compteur = compteur + 1; // Ajout de 1 à la valeur de moyenne
    if (compteur == 10){
        compteur = 0; // Remise à '0' du compteur si la valeur est égale à 10
    }

    delay(100);

}