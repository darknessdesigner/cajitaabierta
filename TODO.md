
DONE

====

DONE * produce guide for how to use with hydra

DONE * validate (compiles and works as expected) 0.1_Blink (tous)

DONE * validate (compiles and works as expected) 0.1_Communication_serielle (tous)

DONE * 0.1_0_Fade Tester sans la librairie analogWrite...celle-ci est maintenant incluse / (Marie-Pier)

DONE * validate (compiles and works as expected) 0.1_DC_moteur_progressif (Marie-Pier)

DONE * validate (compiles and works as expected) 0.1_DC_simple (Marie-Pier)

DONE * validate (compiles and works as expected) 0.1.0_servo (Marie-pier)

DONE * validate (compiles and works as expected) 0.1.0.0_servo (Marie-pier)

DONE * 1.0 Use a limit switch in lieu of the button (Marie-Pier)

DONE * L298N motor control (Marie-Pier)

DONE * Servor motor control (Marie-Pier)



TODO

====

* Add a way to identify individually a cajita (ex 'CajitaAbierta42')
* Improve .bin upload interface
* Add graph to the sensor config page
* Have a ranging routine for mapping of the sensor data 
* Add a 'zeroconf' routine to discover a udp osc service to send to. If 'tdlf' is found, send directly to it
* Re-organize examples (number them by category and between introduction and advanced) / (Maude)
* Add the 3D files for kicad, sort them all out and produce a 3D model of the assembled cajita / (Marie-Pier)
* Do a guide on how to install visual studio code + platformio + git / (Gabrielle et Alexandre)
* Learn how to use the commit feature on github / (Alexandre)

* Examples :

* Create a series of basic examples. Include showing data on the screen : / (Alexandre)
* 1.Button input - Use the ESP32's right button to demo user input / (Marie-Pier)
* 2.Potentiometer (pins 25 and 26) (Gabrielle)
* 3.Any i2c sensor (james ?)
* 4.Accelerometer (Advanced Maude)

* 6.Stepper motor control (Gabrielle)

* 8.Do a drawing game for the LCD screen (?)
* 9.DHT11 Humidity and Temperature sensor (Advanced Maude)


* validate (compiles and works as expected) 0.1_Augmenter_la_valeur_d_une_variable (Maude)
* validate (compiles and works as expected) 0.1_compteur (maude)
* validate (compiles and works as expected) 0.1.0_Fade (Maude)
* validate (compiles and works as expected) 0.10.1_neopixels (James)
* validate (compiles and works as expected) 0.1.1_Fade (Maude)
* validate (compiles and works as expected) 0.1.1_Random (Maude)
* validate (compiles and works as expected) 0.1.2_Fade (Maude)
* validate (compiles and works as expected) 0.1.2_Map (Maude) photorésistence
* validate (compiles and works as expected) 0.1.3_analogRead on pin 33 and serial output (Maude) photorésistence avant 50
* validate (compiles and works as expected) 0.1.3_simple_neopixel_test (James)
* validate (compiles and works as expected) 0.1.4_Trouver_maximum_et_minimum_dans_un_tableau (Gabrielle)
* validate (compiles and works as expected) 0.2_tableau(Gabrielle)
* validate (compiles and works as expected) 0.2.1_tableau_moyenne_des_valeurs(Gabrielle)
* validate (compiles and works as expected) 0.2. 2_tableau_moyenne_des_valeurs_generees_au_hasard (Maude)
* validate (compiles and works as expected) 0.3.1_FilteredADC (Alexandre)
* validate (compiles and works as expected) 0.4.1_BPM_to_pulse_sensor (Alexandre)
