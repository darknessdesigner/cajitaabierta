#include <Arduino.h>

const int patatePin = 12;    // Déclaration des broches // Broche où le limit switch est connecté
const int ledPin = 2;        // Broche de la LED intégrée
const int moteur1 = 32; 
const int moteur2 = 33; 
int buttonState = 0; // Variable pour stocker l'état du bouton

void setup() {
  pinMode(patatePin, INPUT);   // Initialiser la broche de la switch comme entrée
  pinMode(moteur1, OUTPUT);   // Initialiser les broches du contrôleur de moteur comme sortie
  pinMode(moteur2, OUTPUT);
  pinMode(ledPin, OUTPUT);  // Initialiser la broche de la LED comme sortie
  Serial.begin(9600);
}

void loop() {
  buttonState = digitalRead(patatePin);  // Placer l'état du bouton dans la variable buttonState
  Serial.println(buttonState);
  if (buttonState == HIGH) {   // Vérifier si on appuie sur le bouton
    digitalWrite(moteur1,LOW);  // Fait tourner le moteur en direction horaire
    digitalWrite(moteur2, HIGH);
    digitalWrite(ledPin, HIGH); // Allumer la LED intégrée
  } else {
    digitalWrite(moteur1, HIGH); // Direction anti-horaire
    digitalWrite(moteur2, LOW);
    digitalWrite(ledPin, LOW); // Éteindre la LED intégrée
  }
  delay(100);
}