#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

// 0.1.3 // Fonction utile : analogRead()

int sensorPin = 25;   // Choisir la broche d'entrée sur le Esp8266
int sensorValue = 0;  // La variable dans laquelle écrire la valeur mesurée par analogRead()

void setup() {
  Serial.begin(115200);
}

void loop() {
 
  sensorValue = analogRead(sensorPin);
  Serial.print("La valeur du capteur est : ");
  Serial.println(sensorValue);
  delay(100);

}
