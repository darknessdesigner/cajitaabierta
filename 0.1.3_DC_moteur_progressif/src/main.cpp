#include <Arduino.h>

// Image : https://wiki.eavmuqam.ca/moteur_dc_branchements_avec_un_controleur_l298n_et_arduino
// Exemple précédent : Contrôle de base d'un moteur DC (Direct Current).  0.1_DC_moteur_simple
// Faire varier la vitesse du moteur avec la fonction analogWrite

int monPot = 0;

void setup() {
  // initialiser les broches 32 et 33 sont configurées en tant que sorties
  pinMode(32, OUTPUT);
  pinMode(33, OUTPUT);
  pinMode(25, INPUT);
  Serial.begin(115200);
}

void loop(){
  // Nous envoyons une tension positive et l'autre est à 0V, ceci fera tourner le moteur en une direction
  monPot = analogRead(25);
  monPot = map(monPot, 0, 4095, 0, 255);
  digitalWrite(33, LOW); // <-- Tension de 0V
  analogWrite(32, monPot);
  Serial.println("BRAVE NEW world");
 } 

 // Partie du devoir : Comment faire correspondre la vitesse du moteur à la lecture d'un potentiomètre?

