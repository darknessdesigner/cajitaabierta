#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

/*
Ce code montre comment augmenter la valeur d'une variable à chaque passage dans la boucle
Exercice : Utiliser la valeur de la variable pour faire varier l'intensité d'une DEL
Exercice : Faire varier la vitesse de rotation d'un moteur pas à pas en fonction de maVariable
*/

int maVariable = 0; // Déclare une variable de type int, de nom 'maVariable' et instanciée avec la valeur '0'
  
void setup() {
  Serial.begin(115200);   // initialisation de la communication sérielle avec la vitesse de 115200 Bauds
}
 
void loop() {
  Serial.print("La valeur de ma variable est : "); // affiche de texte a l'interieur des guillements 
  Serial.println(maVariable);
  maVariable = maVariable + 1; // Remplace la valeur précédente en y ajoutant '1'
  delay(2000);  
}
