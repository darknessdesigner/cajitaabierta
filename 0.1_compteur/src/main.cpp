#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

// 0.1 Compteur du nombre d'itérations dans la boucle 'loop()'
// Exercice : Changez le nombre d'itérations avant de remettre la variable 'compteur' à zéro.

int compteur = 0; // Déclaration d'une variable 'compteur', initialisée à '0'

void setup() {
  Serial.begin(115200);
}

void loop() {

  Serial.print("La valeur du compteur est : ");
  Serial.println(compteur);

  compteur++;

  if (compteur == 5) { // Lorsque le compteur arrive à 5, remettre à zéro
    compteur = 0;
  }

  delay(500);

}
