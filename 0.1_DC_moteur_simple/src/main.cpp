#include <Arduino.h>

// Image : https://wiki.eavmuqam.ca/moteur_dc_branchements_avec_un_controleur_l298n_et_arduino
// Contrôle de base d'un moteur DC (Direct Current).  
// Pour faire fonctionner un moteur DC, il faut utiliser un contrôleur de moteur L298N

void setup() {
  // initialiser les broches 32 et 33 sont configurées en tant que sorties
  pinMode(32, OUTPUT);
  pinMode(33, OUTPUT);
}

void loop(){
  // Nous envoyons une tension positive et l'autre est à 0V, ceci fera tourner le moteur en une direction
  digitalWrite(32, HIGH);  // <-- Tension de 3.3V  ('HIGH' équivaut à ce voltage)
  digitalWrite(33, LOW); // <-- Tension de 0V

  delay(2000); 

  digitalWrite(32, LOW);  // <-- Tension de 0V  
  digitalWrite(33, LOW); // <-- Tension de 0V

  delay(2000);

  digitalWrite(32, LOW);  // <-- Tension de 0V
  digitalWrite(33, HIGH); // <-- Tension de 3.3V

  delay(2000);

  digitalWrite(32, LOW);  // <-- Tension de 0V 
  digitalWrite(33, LOW); // <-- Tension de 0V

  delay(2000);


} 

// Exercice : Tentez d'arrêter le moteur, de faire une pause, puis de le faire tourner dans l'autre direction
// Voir l'exemple suivant : 0.1.2_DC_moteur_direction
