#include <Arduino.h>

// Exemple, moteur 'pas à pas' (stepper) à 4 fils, connexions pour le LN95
// Identifiez au préalable les paires de fils qui forment les bobines (coils) d'induction


 void setup() {
  // initialiser les broches 4, 32, 0 et 33 en tant que sorties
  pinMode(32, OUTPUT); // Paire A
  pinMode(14, OUTPUT); // Paire B
  pinMode(33, OUTPUT); // Paire A
  pinMode(13, OUTPUT); // Paire B
}

void loop(){
  // Nous envoyons une tension positive et trois sont à 0V, ceci fera un 'pas' du moteur
  digitalWrite(32, LOW); // <-- Tension de 0V
  digitalWrite(14, HIGH);  // <-- Tension de 3.3V  ('HIGH' équivaut à ce voltage)
  digitalWrite(33, HIGH); // <-- Tension de 0V
  digitalWrite(13, LOW);  // <-- Tension de 0V

  delay(20); // Le délai entre chaque pas, modifiez ceci pour accélérer ou ralentir la vitesse de rotation.

  digitalWrite(32, LOW); // <-- Notez le changement dans la séquence, c'est maintenant la seconde broche qui est à 3.3V
  digitalWrite(14, LOW);   
  digitalWrite(33, HIGH);
  digitalWrite(13, HIGH);

  delay(20);

  digitalWrite(32, HIGH);
  digitalWrite(14, LOW); 
  digitalWrite(33, LOW);  
  digitalWrite(13, HIGH);

  delay(20);

  digitalWrite(32, HIGH);
  digitalWrite(14, HIGH);   
  digitalWrite(33, LOW);
  digitalWrite(13, LOW);

  delay(20);   
}
