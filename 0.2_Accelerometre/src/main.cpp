#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>

Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

int accelXValue;
int accelYValue;

void updateAccelerometerInput(){
   
        sensors_event_t event; 
        accel.getEvent(&event);
        /* Display the results (acceleration is measured in m/s^2) */
        //Serial.printf("X: "); Serial.print(event.acceleration.x); Serial.print("  ");
        //Serial.printf("Y: "); Serial.print(event.acceleration.y); Serial.print("  ");
        //Serial.printf("Z: "); Serial.print(event.acceleration.z); Serial.print("  ");Serial.println("m/s^2 ");
        // logger.printf("accel/x :%f\n", event.acceleration.x);
        // logger.printf("accel/y :%f\n", event.acceleration.y);
        // logger.printf("accel/z :%f\n", event.acceleration.z); 
        accelXValue = atan(event.acceleration.y / sqrt(pow(event.acceleration.x, 2) + pow(event.acceleration.z, 2))) * 180 / PI;
        accelXValue = map(accelXValue,-87,88,0,127);
        //Serial.print("accelXValue : ");Serial.println(accelXValue);
        // set IntValue
        //valueX.v = accelXValue;
        accelYValue = atan(-1 * event.acceleration.x / sqrt(pow(event.acceleration.y, 2) + pow(event.acceleration.z, 2))) * 180 / PI;
        accelYValue = map(accelYValue,-85,90,127,0);
        //valueY.v = accelYValue;
        //Serial.printf("valueX.v : %03i, valueY.y : %03i \n",valueX.v, valueY.v);
}

void setup() {
  Serial.begin(115200);
  if(!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no ADXL345 detected ... Check your wiring!");
    // Continue and 'unset accelerometer' // Set a flag to not read the accelerometer?
    // while(1);
  } else { 
        accel.setRange(ADXL345_RANGE_2_G);
        Serial.print  ("Range:         +/- "); 
    
        switch(accel.getRange())
        {
            case ADXL345_RANGE_16_G:
            Serial.print  ("16 "); 
            break;
            case ADXL345_RANGE_8_G:
            Serial.print  ("8 "); 
            break;
            case ADXL345_RANGE_4_G:
            Serial.print  ("4 "); 
            break;
            case ADXL345_RANGE_2_G:
            Serial.print  ("2 "); 
            break;
            default:
        Serial.print  ("?? "); 
        break;
        }  
    Serial.println(" g");  
    Serial.println("accel setted upped");
    Serial.print("Accel data rate : ");
    Serial.println(accel.getDataRate());
    }
}

void loop() {
  updateAccelerometerInput();
  Serial.print(">accelXvalue:"); Serial.println(accelXValue);
  Serial.print(">accelYvalue:"); Serial.println(accelYValue);
  Serial.println("");
  delay(100);
}
