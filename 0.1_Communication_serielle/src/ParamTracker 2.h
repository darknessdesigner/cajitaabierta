#ifndef PARAM_TRACKER_H
#define PARAM_TRACKER_H

#include "ESParam.h"
#include "Printer.h"

#define TRACKER_BUFFER_SIZE 10000
#define TRACKER_COUNT 4
#define TRACKER_BANK_COUNT 9

// place all parameter setup in here
void setupParamTracker(ParamCollector * _pc);
void initTracker();
void updateParamTracker();

#endif