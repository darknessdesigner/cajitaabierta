#include <Arduino.h>
#include "ESParam.h" 
#include "ParamTracker.h"
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <cstdio>
#include "Printer.h"

// void ledCallback(bool i){
//     digitalWrite(LED_BU);
//     printer.println(i?"[pr] led on": "[pr] led off");
// }

void updateOledHeader(){
    sprintf(printer.headerAText, "%s-%s", getDeviceName(), getDeviceSSID());
    sprintf(printer.headerBText, "%s", getDeviceIp());
}
/*
Ce code montre comment on peut utiliser la fonction série pour recevoir des messages
(n'oubliez pas d'ouvrir le moniteur série (l'icône de prise électrique en bas à gauche)
*/

// Exercice : affichez un message lorsque la DEL de 'blink' est allumée
  
void setup() {
  Serial.begin(9600);   // initialisation de la communication sérielle avec la vitesse de 115200 Bauds
  printer.begin();
  setupEsparam(&printer);
  printer.println("[boot] done booting");
  updateOledHeader();
}
 
void loop() {
  Serial.println("Bonjour le monde, halo welt, holá mundo, hello world"); // affiche de texte a l'interieur des guillemets 
  delay(2000); 
  sprintf(printer.headerAText, "%s", "hello esp32");
  sprintf(printer.headerBText, "%i", 42);
  // update the oled screen
  printer.update(2); 
  updateOledHeader();
  updateParamTracker();
}

