/**
 * ##copyright##
 * See LICENSE.txt
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.05
 * @since     2022-03-23
 */
#ifndef PRINTER_H
#define PRINTER_H
#include "Arduino.h"
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <cstdint>

#define OLED_SDA_PIN 21
#define OLED_SCL_PIN 22

#define I2C_ADDRESS 0x3C
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define PRINTER_HEADER_LENGTH 21
#define WEB_LOG_MAX_LENGTH 256

#define SCREEN_SAVER_TIMEOUT 10000
#define LOG_LINE_ROW_COUNT 7
#define MAX_LOG_LINE_WIDTH 21

extern size_t webLogIdx;
extern char webLog[WEB_LOG_MAX_LENGTH];


// multi purpose display
class Printer : public Print {
    public:
        Printer();
        void begin();
        size_t write(uint8_t _c);
        void setHeaderA(const char* _header);
        void setHeaderB(const char* _header);

        void update(uint8_t _s);
        // using flush to call update
        // this way other code not dependent on cajita 
        // can update the oled
        void flush();
        // char headerB[U8LOG_WIDTH];

        // uint8_t u8log_buffer[U8LOG_WIDTH*U8LOG_HEIGHT];
        bool useOled;
        bool useSerial;
        bool useWebLog;
        bool newData;
        bool screenSaver;
        char headerAText[PRINTER_HEADER_LENGTH];
        char headerBText[PRINTER_HEADER_LENGTH];
        unsigned long screenSaverTimeStamp;
        void wakeDisplay();
        void clearWebLog();
        size_t webLogAvailable();
    private:
};

extern Printer printer;
#endif
