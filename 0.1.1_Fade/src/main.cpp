#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

/*
Fade avec limite supérieure en introduisant la notion de condition logique
Vous aurez besoin d'installer la librairie 'polyfill analogWrite for ESP32'
*/
// See analoWrite not being necessary...
#include <analogWrite.h> // From the polyfill analogWrite library for ESP32 //

int luminosite = 0;    // Une variable pour changer la luminosité de la DEL

void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Notre diode bleue sur la plaquette ESP32
  Serial.begin(115200);
}

void loop() {
  analogWrite(LED_BUILTIN, luminosite); // Change la valeur de luminosité de la DEL en utilisant une variable

  luminosite = luminosite + 2; // Changement de la prochaine valeur de luminosité, on ajoute '2' à chaque boucle

  // Si la valeur de luminosité est plus grande de 255, alors remettre la valeur de luminosité à '0'
  if (luminosite > 255) { // Le code entre les accolades est exécuté seulement lorsque la condition est vraie.
    luminosite = 0;
  }

  Serial.print("La valeur de luminosité est : "); // Ouvrez le moniteur série pour lire la valeur de 'luminosite' à chaque passage dans la boucle
  Serial.println(luminosite);
 
  delay(30); // Courte pause pour voir l'effet de tamisage 
}
