#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

// 0.1.4 // Trouver les valeurs minimales et maximales d'un tableau de valeurs

// Utile, entre autres, pour fixer des bornes inférieures et supérieures à map()

int monTableau[7] = {42, 100, 200, 666, 345, 13, 27}; // Déclaration d'un tableau contenant 7 valeurs
int monMax = 0;
int monMin = 1024; // valeur arbitraire qui sera remplacée

void setup() {
  Serial.begin(115200);
}

void loop() {

  for (int i = 0; i < (sizeof(monTableau) / sizeof(monTableau[0])); i++) {

    if (monMin > monTableau[i]) {
      monMin = monTableau[i];
    }

    if (monMax < monTableau[i]) {
      monMax = monTableau[i];
    }

  }

  Serial.print("La valeur de monMin est : ");
  Serial.println(monMin);

  Serial.print("La valeur de monMax est : ");
  Serial.println(monMax);

  Serial.println("--------");

  delay(2000);

}
