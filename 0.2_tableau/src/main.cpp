#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

// 0.2.1 // Faire une moyenne avec les valeurs d'un tableau

// Exercice : Écrire une nouvelle valeur dans le tableau à la position '0' lorsque le compteur est à '5'

int compteur = 0;
int monTableau[5] = {42, 0, 2, 3, 666}; // déclaration d'un tableau contenant 5 valeurs

void setup() {
  Serial.begin(115200);
}

void loop() {

  Serial.print("La valeur à la position : ");
  Serial.print(compteur);
  Serial.print(" est : ");
  Serial.println(monTableau[compteur]);

  compteur++;

  if (compteur == 5) {
    compteur = 0;
  }

  delay(500);

}
