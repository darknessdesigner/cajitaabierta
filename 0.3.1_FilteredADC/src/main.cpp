#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

/*
 * This example reads ADC input and filters it with two separate EWMA filters, each one 
 * with a different smoothing factor. 
 * 
 * You can observe how the first one is faster to detect changes, but more prone to noise, 
 * while the last one is less prone to noise, but slower to detect changes. 
 * 
 * The selection of the best alpha value depends on your actual application, the amount of
 * noise and sampling frequency.
 */
 
#include <Arduino.h>
#include <Ewma.h>

Ewma adcFilter1(0.1);   // Less smoothing - faster to detect changes, but more prone to noise

void setup()
{
    Serial.begin(115200);
    pinMode(A0, INPUT);
}

void loop()
{
    int raw = analogRead(A0);
    float filtered1 = adcFilter1.filter(raw);
 
    Serial.println(raw);
    Serial.print(",");

    Serial.print(filtered1);
    Serial.print(",");
 
    delay(100);
}
