#ifndef ANALOG_INPUT_H
#define ANALOG_INPUT_H

#include "ESParam.h"

extern int sensorAValue; // pour des variables globales
extern int sensorBValue; // pour des variables globales
extern int sensorCValue; // pour des variables globales
extern int sensorDValue; // pour des variables globales

// place all parameter setup in here
void setupAnalogInput(ParamCollector * _pc);

void updateAnalogInput();


#endif