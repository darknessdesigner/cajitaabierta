#include "DHT11.h"

float tempValue; int tempChan; int tempCC; 
float humValue; int humChan; int humCC; 

namespace { // pour setter la valeur du capteur à travers l'interface
    BoolParam valueDHT11Enable;
    FloatParam valueTemp;
    FloatParam valueHum;
}

    DHT dht(DHTPIN, DHTTYPE);

void setupDHT11Input(ParamCollector * _pc){
    valueDHT11Enable.set("/DHT11/enable", 1); // (true, false, true) bool?
    valueDHT11Enable.saveType = SAVE_ON_REQUEST;
    _pc->add(&valueDHT11Enable);

    // valueTemp.set("/temperature/value", 19.0, -25.0, 45.0);
    // valueHum.set("/humidity/value", 19.0, -25.0, 45.0);

    // _pc->add(&valueTemp);
    // _pc->add(&valueHum);

    dht.begin();
    Serial.println("DHT11 ready");

}

void updateDHT11Input(){

    Serial.print("valueDHT11Enable : "); Serial.println(valueDHT11Enable.v);

    if(valueDHT11Enable.v){
        tempValue = dht.readTemperature();
        humValue = dht.readHumidity();

        valueTemp.v = tempValue;
        valueHum.v = humValue;

        // Check if any reads failed and exit early (to try again).
        if (isnan(tempValue) || isnan(humValue)){
            Serial.println(F("Failed to read from DHT sensor!"));
            return;
        }
    }   
}