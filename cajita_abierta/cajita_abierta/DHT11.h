#ifndef DHT11_H
#define DHT11_H

#include "ESParam.h"
#include <Wire.h>
#include <DHT.h>

#define DHTPIN 33
#define DHTTYPE DHT11   // DHT 11

extern float tempValue; // pour des variables globales // ... ou float? 
extern float humValue;

// place all parameter setup in here
void setupDHT11Input(ParamCollector * _pc);

void updateDHT11Input();

#endif