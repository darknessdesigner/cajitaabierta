#ifndef PRESSURESENSOR_H
#define PRESSURESENSOR_H

#include "ESParam.h"
#include <Wire.h>

extern int pressureBlowValue; 
extern int pressureSuckValue;

// place all parameter setup in here
void setupPressureSensorInput(ParamCollector * _pc);

void updatePressureSensorInput();

#endif