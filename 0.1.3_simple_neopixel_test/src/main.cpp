#include <Arduino.h>
#include <Wire.h>

// Exemple encore plus simple

#include <Adafruit_NeoPixel.h>

#define PIN        33 // Broche de votre ESP32 (A4 ou A5)

#define NUMPIXELS 13 // Combien de Néopixels ?

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  pixels.begin(); // INITIALIZER l'objet NeoPixel 
  Serial.begin(115200);
}

void loop() {
  pixels.clear(); // Initialiser tous les pixels à '0'

  for(int i=0; i<NUMPIXELS; i++) { // Pour chaque pixel dans notre bande...
    Serial.print("Nous sommes au pixel numéro : ");
    Serial.println(i);

    // pixels.Color() prends les valeurs RVB (RGB), de 0,0,0 à 255,255,255
    pixels.setPixelColor(i, pixels.Color(255, 0, 0));

    pixels.show();   // Envoie les valeurs aux pixels, ceci est nécessaire!

    delay(200); // Pause entre le itérations de la boucle
  }
}
