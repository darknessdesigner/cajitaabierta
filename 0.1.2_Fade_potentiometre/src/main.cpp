#include <Arduino.h>

// Voir l'exemple précédent 0.1_Fade 

int monPot = 0; // variable pour garder en mémoire la valeur du potentiomètre

void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Notre diode bleue sur la plaquette ESP32
  pinMode(25, INPUT); // Configure la broche du potentiomètre en tant qu'entrée
  Serial.begin(115200);
}

void loop() {
  Serial.print("La valeur de monPot est : ");
  Serial.println(monPot);
  monPot = analogRead(25); // lit la valeur de la broche 25 avec analogRead et attribue-la à monPot
  analogWrite(LED_BUILTIN, monPot); // Change la valeur de luminosité de la DEL en lui passant la valeur de monPot
  delay(100); // Courte pause 
}

// Pourquoi est-ce que la DEL fait un 'fade in' plusieurs fois?
// Comment faire correspondre le mouvement complet de gauche à droite du potentiomètre 
// à l'affichage de la DEL de éteinte à son maximum? 
// Voir l'exemple suivant 0.1_Fade_map 

