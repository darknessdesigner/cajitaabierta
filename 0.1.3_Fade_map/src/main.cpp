#include <Arduino.h>

// Voir l'exemple précédent 0.1_Fade_potentiometre

int monPot = 0; // variable pour garder en mémoire la valeur du potentiomètre

void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Notre diode bleue sur la plaquette ESP32
  pinMode(25, INPUT); // Configure la broche du potentiomètre en tant qu'entrée
  Serial.begin(115200);
}

void loop() {
  monPot = analogRead(25); // lit la valeur de la broche 25 avec analogRead et attribue-la à monPot
  Serial.print("La valeur de monPot est : ");
  Serial.println(monPot);

  monPot = map(monPot, 0, 4095, 0, 255); // Ré-échelonner les valeurs entre deux nouvelles valeurs
  
  Serial.print("La valeur après 'map' de monPot est : ");
  Serial.println(monPot);
  analogWrite(LED_BUILTIN, monPot); // Change la valeur de luminosité de la DEL en lui passant la valeur de monPot
  delay(100); // Courte pause 
}

// Pouvez-vous faire varier la vitesse de rotation d'un moteur DC selon la valeur du potentiomètre?
// Voir : contrôle d'un moteur 0.1_DC_simple