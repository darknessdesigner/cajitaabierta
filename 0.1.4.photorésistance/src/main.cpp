#include <Arduino.h>

// 0.1.4 // Fonction utile : analogRead() pour lire la valeur de la photorésistance

int sensorPin = 33;   // Choisir la broche d'entrée sur le Esp8266
int sensorValue = 0;  // La variable dans laquelle écrire la valeur mesurée par analogRead()

void setup() {
  Serial.begin(115200);
}

void loop() {
 
  sensorValue = analogRead(sensorPin);
  Serial.print(">La valeur du capteur est:");
  Serial.println(sensorValue);
  delay(25);

}
