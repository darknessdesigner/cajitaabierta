#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// Définition des broches
const int buttonPin = 0;    // Broche à laquelle le bouton est connecté
const int ledPin = 2;       // Broche à laquelle la LED est connectée (LED intégrée sur GPIO 2)

// Variables
int buttonState = 0;        // Variable pour lire l'état du bouton
int pot1 = 0;
int pot2 = 0;

///////// DISPLAY ////////
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_SDA_PIN 21
#define OLED_SCL_PIN 22
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
//////////////////////////

void setup() {
  ///////////// DISPLAY /////////////  
  Wire.begin(OLED_SDA_PIN, OLED_SCL_PIN);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  // Affiche 'Cajita Abierta'
  display.clearDisplay();
  display.setTextSize(1);             
  display.setTextColor(WHITE);        
  display.setCursor(0, 0); // Coordonnées d'affichage du texte
  display.print(F("Cajita Abierta v0.9"));
  display.display();
  delay(2000); // Pause de 2 secondes

  // Initialisation de la LED comme sortie
  pinMode(ledPin, OUTPUT);

  // Initialisation du bouton comme entrée avec un pullup interne
  pinMode(buttonPin, INPUT_PULLUP);

  // Assurez-vous que la LED est éteinte au démarrage
  digitalWrite(ledPin, LOW);
  Serial.begin(115200);
}

void loop() {
  display.clearDisplay();
  display.setTextSize(1);             
  display.setTextColor(WHITE);       
  display.setCursor(0, 0);
  display.print(F("Cajita Abierta v0.9"));
  display.setTextSize(2);
  display.setTextColor(WHITE);
  
  // Lire l'état du bouton
  buttonState = digitalRead(buttonPin);

  // Si le bouton est pressé (LOW), allumer la LED
  if (buttonState == LOW) {
    digitalWrite(ledPin, HIGH);
    Serial.println("DEL allumée"); 
  } 
  // Sinon, éteindre la LED
  else {
    digitalWrite(ledPin, LOW);
    Serial.println("DEL éteinte"); 
  }

    pot1 = analogRead(25);
    pot2 = analogRead(26);
    //pot1 = map(pot1,0,4095,0,255);
    //pot2 = map(pot2,0,4095,0,255);
    Serial.println(pot1);
    Serial.println(pot2);
    display.setCursor(0, 26);
    display.println(pot1);
    display.setCursor(60, 26);
    display.println(pot2);
    display.display();


  delay(100);

}