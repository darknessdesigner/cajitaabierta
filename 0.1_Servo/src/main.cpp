#include <Arduino.h>
#include <ESP32Servo.h> // Il faut installer la librairie ESP32servo au préalable
Servo myservo; // Déclare un objet servo

void setup() {
	Serial.begin(115200);
}

void loop() {
	if (!myservo.attached()) {
		myservo.setPeriodHertz(50); // Les servo moteurs 'communiquent' à une certaine fréquence (50hz est un standard). 
		myservo.attach(32, 1000, 2000); // Nous devons connecter la broche 32 (A4) au servo moteur
	}

	  myservo.write(255); // Déplace l'arbre de 60 degrés.
   		delay(2000);
	  myservo.write(0); // Ramène l'arbre à la position initiale
		delay(3000);
			
    Serial.println("Bonjour servo");
}
