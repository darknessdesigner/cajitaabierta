#include <Arduino.h>
//#include <Wire.h>
//#include <Adafruit_GFX.h>

#include <ESP32Servo.h> // Il faut installer la librairie ESP32servo au préalable
Servo myservo1; // Déclare un objet servo
Servo myservo2; // deuxieme servo 
int MonPot = 0; 

void setup() {
	Serial.begin(115200);
	pinMode(25, INPUT); 
}

void loop() {
	if (!myservo1.attached()) {
		myservo1.setPeriodHertz(50); // Les servo moteurs 'communiquent' à une certaine fréquence (50hz est un standard). 
		myservo1.attach(23, 1000, 2000); // Nous devons connecter la broche 32 (A4) au servo moteur
	}

	if (!myservo2.attached()) {
		myservo2.setPeriodHertz(50); // Les servo moteurs 'communiquent' à une certaine fréquence (50hz est un standard). 
		myservo2.attach(19, 1000, 2000); // Nous devons connecter la broche 32 (A4) au servo moteur
	}
	
	MonPot = analogRead (25);
	MonPot = map(MonPot, 0, 4095, 0, 175);
	Serial.print("servo 1 :");Serial.println(MonPot);
	Serial.print("servo 2 :");Serial.println(175-MonPot);
	myservo1.write(MonPot); // Déplace l'arbre selon la valeur du potentiomètre
	myservo2.write(175-MonPot);
	Serial.println("");
	delay(10);		
}
