#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

/*
Fade avec limite supérieure en introduisant la notion de condition logique
Vous aurez besoin d'installer la librairie 'polyfill analogWrite for ESP32'
*/


int luminosite = 0;    // Une variable pour changer la luminosité de la DEL
int augmentationReduction = 2;  // Nous avons besoin d'une seconde variable dont la valeur changera de '2' à '-2' lorsque nous arrivons à une limite des valeurs de DELs ('0' et '255')

void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Notre diode bleue sur la plaquette ESP32
  Serial.begin(115200);
}

void loop() {
  analogWrite(LED_BUILTIN, luminosite); // Change la valeur de luminosité de la DEL en utilisant une variable

  luminosite = luminosite + augmentationReduction; // Changement de la prochaine valeur de luminosité, on ajoute '2' à chaque boucle

  // Si la valeur de luminosité est plus grande de 255, alors faire en sorte de soustraire '2' à luminosite à la prochaine boucle
  if (luminosite > 255) { // Le code entre les accolades est exécuté seulement lorsque la condition est vraie.
    augmentationReduction = -2; // La valeur de luminosité se réduira d'autant à chaque passage dans la boucle 
  }
  
  // Si la valeur de luminosité est plus petite ou égale à '0', alors faire en d'ajouter '2' à luminosite à la prochaine boucle
  if (luminosite <= 0) { // Le code entre les accolades est exécuté seulement lorsque la condition est vraie.
    augmentationReduction = 2;  
  }

  Serial.print("La valeur de luminosité est : "); // Ouvrez le moniteur série pour lire la valeur de 'luminosite' à chaque passage dans la boucle
  Serial.println(luminosite);
 
  delay(30); // Courte pause pour voir l'effet de tamisage 
}
