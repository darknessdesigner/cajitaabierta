/* Affiche la valeur du potentiomètre à l'écran */
/* Exercice : Affichez la valeur du second potentiomètre */
/* Exemple suivant 1.1.3, comment réduire le bruit du capteur? */

#include <Arduino.h>
#include <Wire.h> // Communication par i2c
#include <Adafruit_GFX.h> // Librairie graphique pour l'écran
#include <Adafruit_SSD1306.h> // On ajoute la librairie de l'écran SSD1306

// Crée une instance de l'écran
#define OLED_SDA_PIN 21 // Pour la communication i2c
#define OLED_SCL_PIN 22
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

// Variables
int pot1 = 0;
int pot2 = 0;

void setup() {
  Serial.begin(115200); // Initialisation de la communication sérielle

  /// Écran ///
    Wire.begin(OLED_SDA_PIN, OLED_SCL_PIN);
   
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
        Serial.println(F("SSD1306 allocation failed"));
    }
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.println(F("Exemple Potentiometre 1.1.2"));
    display.display();      // Show initial text
    delay(300);
}

void loop() {

    pot1 = analogRead(25);
    pot2 = analogRead(26);

    Serial.print(">pot1:");Serial.println(pot1);
    Serial.print(">pot2:");Serial.println(pot2);

    // Écran //
    display.clearDisplay(); // Efface l'image précédente
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.print(F("1.1.2 Potentiometre"));
    display.setTextSize(2);
    display.setCursor(5,20);
    display.print(F("pot1:"));
    display.setCursor(65,20);
    display.print(pot1);
    display.display();      // Show initial text

    delay(100);

}