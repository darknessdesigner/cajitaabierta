#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

///////// ÉCRAN ////////
#define SCREEN_WIDTH 128 // Largeur de l'écran OLED, en pixels
#define SCREEN_HEIGHT 64 // Hauteur de l'écran OLED, en pixels
#define OLED_SDA_PIN 21 // Pour la communication i2c
#define OLED_SCL_PIN 22
#define OLED_RESET     -1 // Broche de réinitialisation 
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
//////////////////////////

void setup() {
  ///////////// ÉCRAN /////////////  
  Wire.begin(OLED_SDA_PIN, OLED_SCL_PIN);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Le code va s'arrêter ici
  }
  // Affiche 'Cajita Abierta'
  display.clearDisplay();
  display.setTextSize(1);             
  display.setTextColor(WHITE);        
  display.setCursor(0, 0); // Coordonnées d'affichage du texte
  display.print(F("Cajita Abierta v0.9"));
  display.display();
  delay(2000); // Pause de 2 secondes
}

void loop() {
  display.clearDisplay();
  display.setCursor(0, 26);
  display.print(F("Bonjour le monde!"));
  display.display();
  delay(100);
}