#include <Arduino.h>

// Exemple modifié depuis celui de 'Fade' dans les exemples de base d'Arduino

int luminosite = 0;    // Luminosité de la DEL
int step = 5;
int maitrePot = 25; // broche du potentiomètre
int monPot = 0; // variable pour stocker la valeur du potentiomètre

void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Notre diode bleue sur la plaquette ESP32
  pinMode(maitrePot, INPUT); // mode entrée pour la broche 25
  Serial.begin(115200);
}

void loop() {
  monPot = analogRead(maitrePot); // Va lire la valeur du potentiomètre
  luminosite = monPot;
  analogWrite(LED_BUILTIN, luminosite); // Change la valeur de luminosité de la DEL
  // luminosite = luminosite + step; // Changement de la prochaine valeur de luminosité, on ajoute '5' à chaque boucle
  
 /* if (luminosite >= 255) {
    step = -5;
  } 
  
  else if (luminosite <= 0) {
    step = 5;
  }
  */

  Serial.print("La valeur de luminosité est : ");
  Serial.println(luminosite);
  delay(50); // Courte pause pour voir l'effet de tamisage 
}
