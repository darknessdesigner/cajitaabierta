#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

// 0.1.2 // Fonction utile : Map()

// Exercice : Changez les bornes de la fonction

int monCoupDeD = 0;
int monE = 30;

void setup() {
  Serial.begin(115200);
}

void loop() {

  monCoupDeD = random(100);
  monE = map(monCoupDeD, 0, 99, 30, 60); // Ré-échelonner les valeurs entre deux nouvelles valeurs

  Serial.print("La valeur de monCoupDeD est : ");
  Serial.print(monCoupDeD);
  Serial.print(", ");
  Serial.print("La valeur de monEchelonnage est : ");
  Serial.print(monE);
  Serial.println();

  delay(750);

}
