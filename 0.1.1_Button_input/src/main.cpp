#include <Arduino.h>

// Définition des variables
const int buttonPin = 12;    // Broche à laquelle le bouton est connecté
const int ledPin = 2;       // Broche à laquelle la LED est connectée (LED intégrée sur GPIO 2)
int buttonState = 0;        // Variable pour stocker l'état du bouton
const int potPin = 25;   // Broche du potentimètre
int maitrePot = 0; // Variable pour garder en mémoire la valeur du potentiomètre

// Exercice, mettez à l'oeuvre un interrupteur externe.

void setup() {

  pinMode(ledPin, OUTPUT);   // Initialisation de la LED comme sortie
  pinMode(potPin, INPUT); // Initialisation de la broche du potentiomètre comme entrée
  pinMode(buttonPin, INPUT_PULLUP);   // Initialisation du bouton comme entrée avec un pullup interne
  digitalWrite(ledPin, LOW);   // Assurez-vous que la LED est éteinte au démarrage
  Serial.begin(115200); // Initialisation de la communication sérielle

}
      
void loop() {
  
  buttonState = digitalRead(buttonPin); // Lire l'état du bouton
  maitrePot = analogRead(potPin);
  Serial.println(maitrePot);

  if (buttonState == LOW) { // Si le bouton est pressé (LOW), allumer la LED
    digitalWrite(ledPin, HIGH);
    Serial.println("DEL allumée"); 
  } 

  else {   // Sinon, éteindre la LED
    digitalWrite(ledPin, LOW);
    Serial.println("DEL éteinte"); 
  }

  delay(4000); // Petite pause de 100 millisecondes

}