
#include <Arduino.h>
//#include <Wire.h>
//#include <Adafruit_GFX.h>

//  Variables
const int PulseWire = 32;  // Broche du capteur
int Seuil = 469;           // Ajuster avec le moniteur série ou installez 'teleplot' pour mieux visualiser
int Signal;


void setup() {   
  Serial.begin(115200);     
  pinMode(PulseWire, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  analogReadResolution(10);
}

void loop() {
  Signal = analogRead(PulseWire);  
  Serial.println(">Signal:" + String(Signal)); 
  if(Signal > Seuil){                          // If the signal is above 'Seuil', then "turn-on" the on-Board LED.
     digitalWrite(BUILTIN_LED,HIGH);
     Serial.println("Battement");
   } else {
     digitalWrite(BUILTIN_LED,LOW);                //  Else, the sigal must be below, so "turn-off" this LED.
   }
   delay(10);                    
}
