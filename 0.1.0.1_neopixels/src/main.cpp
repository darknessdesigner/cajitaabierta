#include <Arduino.h>

// Exemple d'utilisation des néopixels
#include <Adafruit_NeoPixel.h>

#define PIN 32 // Broche de votre ESP32 qui va contrôler les LEDs 
#define NUMPIXELS 13 // Combien de Néopixels ?

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  pixels.begin(); // Initialiser l'objet NeoPixel 
  Serial.begin(115200);
}

void loop() {
  pixels.clear(); // Mettre tous les pixels à '0'
  pixels.setPixelColor(7, pixels.Color(255, 255, 0)); // mettre le pixel 7 à 255 dans le rouge, 255 dans le vert
  delay(250);
  pixels.setPixelColor(5, pixels.Color(255, 0, 150));
  delay(250);

  for(int i=0; i<NUMPIXELS; i++) { // Pour chaque pixel dans notre bande...
    pixels.clear(); // Mettre tous les pixels à '0'
    Serial.print("Nous sommes au pixel numéro : ");
    Serial.println(i);
    // pixels.Color() prends les valeurs RVB (RGB), de 0,0,0 à 255,255,255
    pixels.setPixelColor(i, pixels.Color(255, 255, 0));
    pixels.show();   // Envoie les valeurs aux pixels, ceci est nécessaire!
    delay(200); // Pause entre le itérations de la boucle
  }
}