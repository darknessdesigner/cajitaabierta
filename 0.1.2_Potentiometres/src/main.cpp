#include <Arduino.h>

// Variables
int pot1 = 0;
int pot2 = 0;

void setup() {
  Serial.begin(115200); // Initialisation de la communication sérielle
}

void loop() {

    pot1 = analogRead(25);
    pot2 = analogRead(26);

    Serial.print(">pot1:");Serial.println(pot1);
    Serial.print(">pot2:");Serial.println(pot2);

  delay(100);

}