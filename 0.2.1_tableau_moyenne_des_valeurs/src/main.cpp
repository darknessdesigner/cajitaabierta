#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

// 0.2.1 // Faire la moyenne des valeurs d'un tableau

// Utile pour lisser les valeurs qui proviennent de capteurs

// Exercice 1 : Ouvrir le traceur série dans 'Outils'
// Exercice 2 : Ajoutez 5 valeurs supplémentaires à 'monTableau'
// Exercice 3 : Enlever les commentaires aux lignes 28 et subséquentes pour afficher la valeur de 'monTotal'

int compteur = 0;
int monTableau[5] = {100, 200, 300, 400, 500}; // déclaration d'un tableau contenant 5 valeurs
int monTotal;
int maMoyenne;

void setup() {
  Serial.begin(115200);
}

void loop() {

  Serial.print("La valeur de MonTableau à : ");
  Serial.print(compteur);
  Serial.print(" est : ");
  Serial.println(monTableau[compteur]);

  // Addition de toutes les valeurs du tableau à l'aide d'une boucle 'for'
  for (int i = 0; i < (sizeof(monTableau) / sizeof(monTableau[0])); i++) {
    monTotal = monTableau[i] + monTotal;
    // Serial.print("monTotal à : ");
    // Serial.print(i);
    // Serial.print(" est : ");
    // Serial.println(monTotal);
    // delay(200);
  }


  maMoyenne = monTotal / (sizeof(monTableau) / sizeof(monTableau[0])); // Fait la moyenne

  monTotal = 0; // Remise de monTotal à 0

  Serial.print("La moyenne est : ");
  Serial.println(maMoyenne);

  compteur++;

  if (compteur == 5) {
    compteur = 0;
  }

  delay(750);

}
