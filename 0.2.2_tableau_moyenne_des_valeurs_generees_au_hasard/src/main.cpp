#include <Arduino.h>
#include "ESParam.h" 
#include <Wire.h>
#include <Adafruit_GFX.h>

// 0.2.2 // Faire la moyenne des valeurs d'un tableau en ajoutant des valeurs généréees au hasard pour simuler un capteur

// Utile pour lisser les valeurs qui proviennent de capteurs

int compteur = 0;
int monTableau[10] = {50, 50, 50, 50, 50, 50, 50, 50, 50, 50}; // Déclaration d'un tableau contenant 10 valeurs
int monTotal;
int maMoyenne;
int monCoupDeD = 0;

void setup() {
  Serial.begin(115200);
}

void loop() {

  monCoupDeD = random(50);

  // Écrire la valeur de "mon coup de dés" (en ajoutant 50) dans monTableau à la position actuelle du tableau indiquée par compteur
  monTableau[compteur] = monCoupDeD + 50; // Va produire des valeurs entre 50 et 99

  Serial.print("La valeur de monTableau[compteur] est : ");
  Serial.println(monTableau[compteur]);

  // Addition de toutes les valeurs du tableau à l'aide d'une boucle 'for'
  for (int i = 0; i < (sizeof(monTableau) / sizeof(monTableau[0])); i++) {
    monTotal = monTableau[i] + monTotal;
  }


  maMoyenne = monTotal / (sizeof(monTableau) / sizeof(monTableau[0])); // Fait la moyenne
  monTotal = 0; // Remise de monTotal à 0

  Serial.print("La moyenne est : ");
  Serial.println(maMoyenne);

  compteur++;

  if (compteur == (sizeof(monTableau) / sizeof(monTableau[0]))) { // Ajuste le compteur au nombre d'éléments du tableau
    compteur = 0;
  }

  delay(250);

}
